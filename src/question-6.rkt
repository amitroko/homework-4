; --------------------------------
; Question 6
; --------------------------------
; fn main() {
;   let s = String::from("hello0123!");
;   let mut z = 0;
;   for c in s.chars() { 
;     match c {
;       'a'...'z' => z += 1,
;       '0'...'9' => z += 2,
;       _ => z += 3,
;     };
;   }
; }

#lang racket

(module test racket 
  (require "assert.rkt")
  ; Add code below
  ; ------------------
(define s (open-input-string "hello0123!"))
  (define z 0)
  (for ([c (in-input-port-chars s)])(cond [(char-numeric? c)(set! z (+ z 2))][(char-alphabetic? c)(set! z (+ z 1))] [(set! z (+ z 3))]))(display z)
  ; ------------------
  ; Add code above
  (assert z 16))
