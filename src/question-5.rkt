; --------------------------------
; Question 5
; --------------------------------
; fn main() {
;   let s = "hello";
;   let x = if s.len() >= 5 && s.get(0..5).unwrap() == "hello" {
;     1 
;   } else {
;     2
;   };
;   println!("{:?}", x);
; }

#lang racket

(module test racket 
  (require "assert.rkt")
  ; Add code below
  ; ------------------
(define s "hello")
(define length (string-length "hello"))
(define x 0)
(set! x (cond [(and(>= length 5)(string=? s "hello"))(+ x 1)][(+ x 2)]))
  ; -----------------
  ; Add code above
  (assert x 1))